Oncotator INFO filter
=========================


A simple script that help to reduce the amount of info in a vcf produced
by [oncotator][1].

>The script works with vcf files from oncotator, other tools could use a different format for the info field.



Help menu
-----------
```
$ python oncotator_vcf_parse.py -h
usage: oncotator_vcf_parse.py [-h] [-l] [-k [KEYS [KEYS ...]]] [-s SUB]
                              [--out OUT]
                              vcf

positional arguments:
  vcf                   VCF file to parse

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            List available INFO keys. This flag will prevent the
                        actual parsing of the input file.
  -k [KEYS [KEYS ...]], --keys [KEYS [KEYS ...]]
                        Keys to keep in the INFO field. If the --list(-l) flag
                        is selected instead of filtering the VCF the content
                        of the selected keys would be displayed. List of keys
                        is submitted without quotes (eg: -k A B C D).
  -s SUB, --subkey SUB  Some keys condense severals values in the INFO filed.
                        With this option it is possible to append a list of
                        subkeys to be selected. The format is "key:subkey". It
                        is possible to call the option several time (eg: -s
                        key1:value1 -s key1:value2, -s key2:value3).
  --out OUT, -o OUT     Output file, default STDOUT
```

Basic example
-------------
Parse the header of a vcf:
```
$ python oncotator_vcf_parse.py exac.vcf -l
Available INFO keys:
ExAC_Het_AMR ExAC_Hemi_EAS ExAC_Hemi_OTH ExAC_culprit ExAC_ReadPosRankSum ExAC_DP_HIST ExAC_DP ExAC_MLEAF ExAC_MLEAC ExAC_AN_OTH ExAC_VQSLOD ExAC_HaplotypeScore ExAC_AC_Adj ExAC_Hemi_FIN ExAC_Hemi_NFE ExAC_AC_AMR ExAC_AN_AFR ExAC_CCC ExAC_AN_NFE ExAC_AC ExAC_AC_FIN ExAC_AF ExAC_AC_AFR ExAC_Hom_AFR ExAC_AC_OTH ExAC_Het_SAS ExAC_AN ExAC_Hom_EAS ExAC_Het_OTH ExAC_ClippingRankSum ExAC_Hom_FIN ExAC_NEGATIVE_TRAIN_SITE ExAC_Hemi_AMR ExAC_BaseQRankSum ExAC_DS ExAC_FS ExAC_MQ ExAC_AN_SAS ExAC_CSQ ExAC_END ExAC_NCC ExAC_AN_EAS ExAC_AN_AMR ExAC_POSITIVE_TRAIN_SITE ExAC_AC_EAS MQ0 ExAC_AN_FIN ExAC_GQ_STDDEV VT ExAC_GQ_MEAN ExAC_MQ0 ExAC_AC_NFE ExAC_Hemi_SAS SOMATIC ExAC_Hom_OTH ExAC_Het_AFR ExAC_Hom_NFE ExAC_HWP ExAC_AC_Hom ExAC_GQ_HIST DB ExAC_Het_FIN ExAC_Het_NFE ExAC_AC_Het ExAC_Hemi_AFR ExAC_AC_SAS ExAC_InbreedingCoeff ExAC_Het_EAS ExAC_DB ExAC_Hom_SAS ExAC_QD ExAC_Hom_AMR ExAC_AC_Hemi SF ExAC_AN_Adj ExAC_MQRankSum
```

Get description from info keys:
```
$ python oncotator_vcf_parse.py exac.vcf -l -k ExAC_Het_AMR ExAC_Hemi_EAS ExAC_Hemi_OTH ExAC_culprit ExAC_ReadPosRankSum ExAC_DP_HIST ExAC_CSQ
ExAC_Het_AMR
	American Heterozygous Counts
ExAC_Hemi_EAS
	East Asian Hemizygous Counts
ExAC_Hemi_OTH
	Other Hemizygous Counts
ExAC_culprit
	The annotation which was the worst performing in the Gaussian mixture model, likely the reason why the variant was filtered out
ExAC_ReadPosRankSum
	Z-score from Wilcoxon rank sum test of Alt vs. Ref read position bias
ExAC_DP_HIST
	Histogram for DP; Mids
		Values: 2.5, 7.5, 12.5, 17.5, 22.5, 27.5, 32.5, 37.5, 42.5, 47.5, 52.5, 57.5, 62.5, 67.5, 72.5, 77.5, 82.5, 87.5, 92.5, 97.5
ExAC_CSQ
	Consequence type as predicted by VEP. Format
		Values: Allele, Gene, Feature, Feature_type, Consequence, cDNA_position, CDS_position, Protein_position, Amino_acids, Codons, Existing_variation, ALLELE_NUM, DISTANCE, STRAND, SYMBOL, SYMBOL_SOURCE, HGNC_ID, BIOTYPE, CANONICAL, CCDS, ENSP, SWISSPROT, TREMBL, UNIPARC, SIFT, PolyPhen, EXON, INTRON, DOMAINS, HGVSc, HGVSp, GMAF, AFR_MAF, AMR_MAF, ASN_MAF, EUR_MAF, AA_MAF, EA_MAF, CLIN_SIG, SOMATIC, PUBMED, MOTIF_NAME, MOTIF_POS, HIGH_INF_POS, MOTIF_SCORE_CHANGE, LoF_info, LoF_flags, LoF_filter, LoF
```

Select subkeys:
```
$ python oncotator_vcf_parse.py exac.vcf -k ExAC_Het_AMR ExAC_Hemi_EAS ExAC_Hemi_OTH ExAC_culprit ExAC_ReadPosRankSum ExAC_DP_HIST ExAC_CSQ -s ExAC_CSQ:HGNC_ID -s ExAC_CSQ:SYMBOL -o test.vcf
$
$ python oncotator_vcf_parse.py test.vcf -l
Available INFO keys:
ExAC_Het_AMR ExAC_Hemi_EAS ExAC_Hemi_OTH ExAC_culprit ExAC_ReadPosRankSum ExAC_DP_HIST ExAC_CSQ
$
$ python oncotator_vcf_parse.py test.vcf -k ExAC_CSQ -l
ExAC_CSQ
	Consequence type as predicted by VEP. Format
		Values: SYMBOL, HGNC_ID
```

TODO
------
There is a lot to improve/fix, I just wrote this code as a proof of concept, but it seems pretty handy and cover the scope for now. I would like to rewrite it with a bit of strategy in mind... but probably I will have no time for it. Feel free to extend it, fix bugs or use it as it is.


[1]: https://www.broadinstitute.org/oncotator


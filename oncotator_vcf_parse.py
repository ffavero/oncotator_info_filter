#!/usr/bin/env python2.7

import re
import argparse
import os
import sys
import gzip


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


def parse_vcf_info(info):
    '''
    Very basic function that convert the INFO fields of a VCF into a 
    python dictionary object
    '''
    info_split = info.split(';')
    parsed_info = {}
    for item in info_split:
        try:
            key, value = item.split('=')
            parsed_info[key] = value
        except ValueError:
            parsed_info[item] = None
    return parsed_info


def get_headline_content(line):
    '''
    Try to get the string enclosed by "< ... >" in the VCF header
    '''
    start = None
    end = None
    counter = 0
    for character in line:
        if start is None:
            if character == '<':
                start = counter + 1
        else:
            if character == '>':
                end = counter
        counter += 1
    if start and end:
        column = line[0:start - 1].strip('##').strip('=')
        content = line[start:end]
        pattern = re.compile(r'''((?:[^,"']|"[^"]*"|'[^']*')+)''')
        content = pattern.split(content)[1::2]
        header_line = {}
        for item in content:
            key, value = item.split('=', 1)
            value = value.strip('"').split(':')
            if len(value) == 2:
                header_line[key] = [value[0], value[1].strip(' ').split('|')]
            else:
                header_line[key] = value[0]
        line_id = header_line.pop('ID', None)
    return (column, line_id, header_line)


def subkeys_expand(subkeys):
    expanded = {}
    if subkeys:
        for item in subkeys:
            key, value = item.split(':')
            if key in expanded.keys():
                expanded[key].append(value)
            else:
                expanded[key] = [value]
    return expanded


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('vcf',  help='VCF file to parse')
    parser.add_argument('-l', '--list',  dest='list', help='List available INFO keys. This flag will \
                                                               prevent the actual parsing of the input file.',
                        action='store_true')
    parser.add_argument('-k', '--keys',  dest='keys', nargs='*',
                        help='Keys to keep in the INFO field. If the --list(-l) flag is selected \
                               instead of filtering the VCF the content of the selected keys would \
                               be displayed. List of keys is submitted without quotes (eg: -k A B C D).', default=[])
    parser.add_argument('-s', '--subkey',  dest='sub', action='append',
                        help='Some keys condense severals values in the INFO filed. With this option \
                               it is possible to append a list of subkeys to be selected. The format is \
                               "key:subkey". It is possible to call the option several time (eg: \
                               -s key1:value1 -s key1:value2, -s key2:value3).')
    parser.add_argument('-f', '--filter',  dest='filt', nargs='*',
                        help='Filter results based on required keys (eg: -k A B C -f B C).', default=[])
    parser.add_argument('--out', '-o', dest='out', type=str, default='-',
                        help='Output file, default STDOUT')

    args = parser.parse_args()

    headers_info = {}
    subkeys = subkeys_expand(args.sub)
    info_column = None
    if args.list:
        args.out = '-'
    else:
        for filter in args.filt:
            if filter not in args.keys:
                sys.exit(
                    "The filter %s in not present in the specified keys" % filter)
    with xopen(args.vcf, 'rt') as file_in, xopen(args.out, 'wt') as file_out:
        for line in file_in:
            if line.startswith('##'):
                # parse the header information
                if line.startswith('##INFO'):
                    headers_line = get_headline_content(line)
                    column = headers_line[0]
                    if column not in headers_info.keys():
                        headers_info[column] = {}
                        headers_info[column][headers_line[1]] = headers_line[2]
                    else:
                        headers_info[column][headers_line[1]] = headers_line[2]
                    if not args.list:
                        if headers_line[1] in args.keys:
                            if headers_line[1] in subkeys.keys():
                                description = headers_info[column][
                                    headers_line[1]]['Description']
                                keep_subkeys = []
                                for subkey in description[1]:
                                    if subkey in subkeys[headers_line[1]]:
                                        keep_subkeys.append(subkey)
                                description = description[
                                    0] + ': ' + '|'.join(keep_subkeys)
                                file_out.write('##' + column + '=<ID=' + headers_line[1] + ',Number=' +
                                               headers_info[column][headers_line[1]]['Number'] + ',Type=' +
                                               headers_info[column][headers_line[1]]['Type'] + ',Description="' +
                                               description + '">\n')
                            else:
                                file_out.write(line)

                else:
                    if not args.list:
                        file_out.write(line)
            elif line.startswith('#'):
                # get the index of the columns
                info_column = line.strip('#').strip().split('\t').index('INFO')
                if not args.list:
                    file_out.write(line)

            else:
                if args.list:
                    if len(args.keys) == 0:
                        file_out.write('Available INFO keys:\n' +
                                       ' '.join(headers_info['INFO'].keys()) + '\n')
                    else:
                        for key in headers_info['INFO']:
                            if key in args.keys:
                                file_out.write(key + '\n')
                                description = headers_info[
                                    'INFO'][key]['Description']
                                if type(description) is list:
                                    file_out.write(
                                        '\t' + description[0] + '\n')
                                    file_out.write(
                                        '\t' + '\t' + 'Values: ' + ', '.join(description[1]) + '\n')
                                else:
                                    file_out.write('\t' + description + '\n')
                    break
                else:
                    line = line.strip().split('\t')
                    info = parse_vcf_info(line[info_column])
                    info_subset = {}
                    for key in args.keys:
                        try:
                            info_subset[key] = info[key]
                        except KeyError:
                            pass
                    info_str = []
                    for key in info_subset:
                        if info_subset[key]:
                            value = info_subset[key]
                            if key in subkeys.keys():
                                values_sub = []
                                for subkey in subkeys[key]:
                                    values_sub.append(value.split('|')[headers_info['INFO'][
                                                      key]['Description'][1].index(subkey)])
                                value = '|'.join(values_sub)
                            info_str.append(key + '=' + value)
                        else:
                            info_str.append(key)
                    to_print = True
                    for filter in args.filt:
                        if filter not in info_subset.keys():
                            to_print = False
                    if to_print:
                        info = ";".join(info_str)
                        line[info_column] = info
                        file_out.write('\t'.join(line) + '\n')

if __name__ == "__main__":
    main()
